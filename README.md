# d3jsample

That's a simple project developed at university for a DataViz course, to become familiar with [d3.js](https://d3js.org/) library. Based on some dataset taken online, creates an interactive chart that shows the relation between CO2 emission and production of nucler energy, to see if there can be a correlation between the two.

### Run the Project

- With `docker`, from the root of the directory run:

	    $ docker build . -t ollaw/d3jsample:0.1
		$ docker run -p 8080:8080 ollaw/d3jsample:0.1
	and then open http://localhost:8080/.

- With `npm`, from the root of the directory run:

        $ npm install
		$ npm run build
		$ npm start
	and then open http://localhost:8080/.