require('bootstrap');
import $ from 'jquery';
import * as d3 from 'd3';

import '../scss/main.scss';


const svgWidth = window.innerWidth * .85;
const svgHeight = window.innerHeight * .5;
const svgMargin = 75;

const graphWidth = svgWidth - 2 * svgMargin;
const graphHeight = svgHeight - 2 * svgMargin;

let d3Dataset;
let filterDataset;
let main;
let svg;
let xAxis;
let gXAxis;
let yAxis;
let gYAxis;
let zoom;
let xScale;
let yScale;
let co2Scale;
let nucScale;
let popScale;

let filteredState = new Set();
let currentYear = 2000;


const colorScale = d3.scaleOrdinal(d3.schemeSet3);

$('.slider').on('input change', function () {
	currentYear = this.value;
	$('#year').text(currentYear);
	updateChart(currentYear);
});


Promise
	.all([d3.csv("resources/carbonDioxideData.csv"), d3.csv("resources/populationData.csv"), d3.csv("resources/nuclearEnergyData.csv")])
	.then((csvArr) => {


		let [co2Csv, popCsv, nuclearCsv] = csvArr;
		popCsv = cleanPopCsv(popCsv);
		let popMap = generatePopulationMap(popCsv);
		let eduMap = generateNuclearMap(nuclearCsv);
		let hclMap = generateCo2Map(co2Csv);
		d3Dataset = createDataset(hclMap, eduMap, popMap);

		createCheckList(d3Dataset);


		const tmpCleanDataset = d3Dataset
			.filter(e => !isNaN(e["CO2"]) && !isNaN(e["Nucl"]));




		// Extract max value of the plastic (Y Axis) 
		let maxY = Math.max(...tmpCleanDataset.map(e => Number(e["Nucl"])));

		// Extract max value of the population (X Axis)
		let maxX = Math.max(...tmpCleanDataset.map(e => Number(e["CO2"])));


		let popArray = tmpCleanDataset.map(e => Number(e["Pop"]));
		let maxZ = Math.max(...popArray);
		let minZ = Math.min(...popArray);




		zoom = d3.zoom()
			.scaleExtent([1, Infinity])
			.on("zoom", zoomed);
		zoom.filter(() => !d3.event.button && d3.event.ctrlKey)
		/* Using zoom only with crtl clicked */

		svg = d3.select("#viz")
			.append("svg")
			.attr("width", svgWidth)
			.attr("height", svgHeight)

			.call(zoom)
			.on("wheel", () => { if (d3.event.ctrlKey) d3.event.preventDefault() })

		main = svg.append('g')
			.attr('width', svgWidth)
			.attr('height', svgHeight)
			.attr("transform", `translate(20,${svgMargin / (-2)})`)

		main.append("defs")
			.append("svg:clipPath")
			.attr("id", "clip")
			.append("svg:rect")
			.attr("id", "clip-rect")
			.attr("x", svgMargin)
			.attr("y", svgMargin)
			.attr('width', graphWidth)
			.attr('height', graphHeight);

		/* --- Y label --- */
		main.append("text")
			.attr("transform", "rotate(-90)")
			.attr('class', 'y label')
			.attr("y", 10)
			.attr("x", - svgHeight / 2)
			.attr("dy", "10px")
			.style("text-anchor", "middle")
			.text(" Electricity produced with nuclear");

		main.append("text")
			.attr("transform", "rotate(-90)")
			.attr('class', 'y label')
			.attr("y", 25)
			.attr("x", - svgHeight / 2)
			.attr("dy", "10px")
			.style("text-anchor", "middle")
			.text("energy (kWh × 10⁶) each	 10³ people")

		/* --- Y label --- */

		/* --- X label --- */
		main.append("text")
			.attr('class', 'y label')
			.attr("y", svgHeight - svgMargin / 2)
			.attr("x", svgWidth / 2)
			.attr("dy", "10px")
			.style("text-anchor", "middle")
			.text("CO2 Emissions (kt) each 10³ people");

		/* --- x label --- */

		main.append("text")
			.attr('class', 'year')
			.attr("y", (svgHeight / 2) + svgMargin / 2)
			.attr("x", (svgWidth / 2))
			.attr("id", "year")
			.style("text-anchor", "middle")
			.text("2000")

		// Scale on x-axis
		co2Scale = d3
			.scaleLinear()
			.domain([0, maxX + 0.1 * maxX])
			.range([svgMargin, svgWidth - svgMargin])

		// Scale on y-axis
		nucScale = d3
			.scaleLinear()
			.domain([0, maxY + 0.1 * maxY])
			.range([svgHeight - svgMargin, svgMargin])

		// Scale for the dimension of the circles 
		popScale = d3
			.scaleLinear()
			.domain([minZ, maxZ])
			.range([10, 50])


		xAxis = d3.axisBottom(co2Scale)
			.ticks(10)
			.tickSize(-graphHeight);

		yAxis = d3.axisLeft(nucScale)
			.ticks(5)
			.tickSize(-graphWidth)


		gXAxis = main.append("g")
			.attr("transform", `translate(0,${svgHeight - svgMargin})`)
			.attr('class', 'x axis')
			.call(xAxis)

		gYAxis = main.append("g")
			.attr("transform", `translate(${svgMargin},0)`)
			.attr('class', 'y axis')
			.call(yAxis);


		drawChart(currentYear);
	});

const drawChart = (year) => {


	filterDataset = d3Dataset
		.filter(e => e["Year"] == year)
		.filter(e => !isNaN(e["Nucl"]))

	main.append("g")
		.attr("clip-path", "url(#clip)")
		.attr("class", "main-g")
		.selectAll("circle")
		.data(filterDataset)
		.enter()
		.append("circle")
		.attr("cx", e => co2Scale(e["CO2"]))
		.attr("cy", e => nucScale(e["Nucl"]))
		.attr("r", e => popScale(e["Pop"]))
		.attr("fill", e => e["Color"])
		.on("mouseover", d => {
			main.selectAll("circle").sort((a, b) => (a == d) ? 1 : -1);
			updatedTextValues(d);
		})

		.append("title")
		.text(e => e["Area"]);

}


const updatedTextValues = (data) => {
	$('#NationText').removeClass('text-muted').text(data['Area']);
	$('#YearText').removeClass('text-muted').text(data['Year']);
	$('#PopulationText').removeClass('text-muted').text(`${Math.trunc(data['Pop'])} (average)`);
	$('#EnergyText').removeClass('text-muted').text(`${data['Nucl'].toFixed(2)} × 10⁶ Wh`);
	$('#CO2Text').removeClass('text-muted').text(`${data['CO2'].toFixed(2)} × 10³ kg`);
	$('#colorText').children(":first").children(":first").css({ fill: data['Color'] });
}

const updateChart = (year) => {

	filterDataset = d3Dataset
		.filter(e => e["Year"] == year)
		.filter(e => !isNaN(e["Nucl"]))
		.filter(e => !filteredState.has(e["Area"]))

	let trns = d3.transition()


	let circles = main.select("g.main-g")
		.selectAll("circle")
		.data(filterDataset, e => e["Area"])

	circles.exit()
		.remove()
		.transition(trns);

	circles
		.enter()
		.append("circle")
		.attr("cx", e => co2Scale(e["CO2"]))
		.attr("cy", e => nucScale(e["Nucl"]))
		.attr("r", e => popScale(e["Pop"]))
		.attr("fill", e => e["Color"])
		.on("mouseover", d => {
			main.selectAll("circle").sort((a, b) => (a == d) ? 1 : -1);
			updatedTextValues(d);
		})
		.append("title")
		.text(e => e["Area"])
		.transition(trns);

	circles
		.transition(trns)
		.attr("cx", e => co2Scale(e["CO2"]))
		.attr("cy", e => nucScale(e["Nucl"]))
		.attr("r", e => popScale(e["Pop"]))

}


const cleanEduCsv = (csv) =>
	csv
		.filter(e => e["Sex"] == "All genders")
		.filter(e => Number(e["Time Period"]) > 1989)
		.filter(e => Number(e["Observation Value"]) > 0)


const cleanPopCsv = (csv) =>
	csv
		.filter(e => e["Variant"] == "Medium")
		.filter(e => Number(e["Year(s)"] > 1989 || Number(e["Year(s)"] < 2016)))


const generatePopulationMap = (popCsv) => {
	const popMap = {};
	const regionSet = new Set(popCsv.map(e => e["Country or Area"]));
	regionSet.forEach(e => {
		let yearMap = Object.assign({}, ...popCsv
			.filter(d => d["Country or Area"] == e)
			.map(s => ({ [s["Year(s)"]]: s["Value"] })
			)
		)
		popMap[e] = yearMap;
	})
	return popMap;
}

const generateNuclearMap = (eduCsv) => {
	const eduMap = {};
	const regionSet = new Set(eduCsv.map(e => e["Country or Area"]));
	regionSet.forEach(e => {
		let yearMap = Object.assign({}, ...eduCsv
			.filter(d => d["Country or Area"] == e)
			.map(s => ({ [s["Year"]]: s["Quantity"] })
			)
		)
		eduMap[e] = yearMap;
	})
	return eduMap;
}


const generateCo2Map = (co2Csv) => {
	const co2Map = {};
	const regionSet = new Set(co2Csv.map(e => e["Country or Area"]));
	regionSet.forEach(e => {
		let yearMap = Object.assign({}, ...co2Csv
			.filter(d => d["Country or Area"] == e)
			.map(s => ({ [s["Year"]]: s["Value"] })
			)
		)
		co2Map[e] = yearMap;
	})
	return co2Map;
}


const createDataset = (co2Map, nuclearMap, popMap) => {
	const co2RegionSet = new Set(Object.keys(co2Map));
	const nuclearRegionSet = new Set(Object.keys(nuclearMap));
	const popRegionSet = new Set(Object.keys(popMap));
	const regionSet = new Set(Array.from(co2RegionSet)
		.filter(e => nuclearRegionSet.has(e))
		.filter(e => popRegionSet.has(e))
	)
	let dataset = [];
	regionSet.forEach(r => {
		for (let k = 1990; k < 2015; ++k) {
			const quotient = Number(popMap[r][k]) / 1000;
			dataset.push({
				"Area": r, "Year": k,
				"Pop": Number(popMap[r][k]),
				"Nucl": Number(nuclearMap[r][k]) / quotient,
				"CO2": Number(co2Map[r][k]) / quotient,
				"Color": colorScale(r)
			})
		}
	})
	return dataset;
}


const zoomed = () => {
	var transform = d3.event.transform;

	transform.x = Math.min(0, Math.max(d3.event.transform.x, svgWidth - svgWidth * d3.event.transform.k));
	transform.y = Math.min(0, Math.max(d3.event.transform.y, svgHeight - svgHeight * d3.event.transform.k));

	var newCo2Scale = transform.rescaleX(co2Scale);
	var newNucScale = transform.rescaleY(nucScale);
	xAxis.scale(newCo2Scale)
	yAxis.scale(newNucScale)
	gXAxis.call(xAxis);
	gYAxis.call(yAxis);

	main.selectAll("circle")
		.attr("cx", e => newCo2Scale(e["CO2"]))
		.attr("cy", e => newNucScale(e["Nucl"]))
		.attr("r", e => popScale(e["Pop"] * d3.event.transform.k))

	main.selectAll("g.main-g")
		.selectAll("text")
		.data(filterDataset)
		.transition()
		.text(e => e["Area"])
		.attr("x", e => newCo2Scale(e["CO2"]))
		.attr("y", e => newNucScale(e["Nucl"]))

}

const createCheckList = (dataset) => {

	Array.from(new Set(dataset.map(e => e["Area"])))
		.map(e => ` <li>
						<label class="checkbox-inline">
							<input type="checkbox" onclick="handleClick(this)" value="${e}" checked>${e}
						</label>
					</li>`)
		.forEach(e => $('.area-list').append(e));
}

const handleClick = (e) => {
	e.checked
		? filteredState.delete(e.value)
		: filteredState.add(e.value);
	updateChart(currentYear);
}

window.selectAll = () => {
	$('.area-list').find('input').toArray().forEach(e => e.checked = true);
	filteredState = new Set();
	updateChart(currentYear);
}


window.selectNone = () => {
	$('.area-list').find('input').toArray().forEach(e => e.checked = false);
	filteredState = new Set(d3Dataset.map(e => e["Area"]));
	updateChart(currentYear);
}

